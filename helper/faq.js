import { CommonModule } from '@angular/common';
import { EventEmitter, Component, Input, Output, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MatFaqAdminComponent {
    constructor() {
        this.title = 'Admin';
        this.onFAQItemAdded = new EventEmitter();
    }
    /**
     * @return {?}
     */
    reset() {
        this.question = this.answer = undefined;
    }
    /**
     * @return {?}
     */
    add() {
        /** @type {?} */
        const faqItem = {
            question: this.question,
            answer: this.answer
        };
        this.onFAQItemAdded.emit(faqItem);
        this.reset();
    }
}
MatFaqAdminComponent.decorators = [
    { type: Component, args: [{
                selector: 'mat-faq-admin',
                template: `
    <mat-toolbar color="primary">{{title}}</mat-toolbar>
    <mat-card>
      <mat-card-content fxLayout="column">
        <mat-form-field class="full-width" appearance="outline">
          <mat-label>The question</mat-label>
          <input matInput [(ngModel)]="question">
          <mat-hint>Add here the question of your clients</mat-hint>
        </mat-form-field>

        <mat-form-field class="full-width" appearance="outline">
          <mat-label>The answer</mat-label>
          <textarea matInput [(ngModel)]="answer"></textarea>
          <mat-hint>Please type an appropriate answer here to the above question</mat-hint>
        </mat-form-field>
      </mat-card-content>

      <mat-card-actions fxLayoutAlign="end">
        <button mat-fab color="primary" (click)="add()">
          <mat-icon>add</mat-icon>
        </button>
      </mat-card-actions>
    </mat-card>
  `,
                styles: [`
    .full-width{width:100%}.mat-form-field{margin:.8rem 0}
  `]
            },] },
];
MatFaqAdminComponent.propDecorators = {
    title: [{ type: Input }],
    onFAQItemAdded: [{ type: Output }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MatFaqComponent {
    constructor() {
        this.title = 'FAQ';
        this.multi = false;
        this.displayMode = 'default'; // or flat
        // or flat
        this.faqList = [];
    }
}
MatFaqComponent.decorators = [
    { type: Component, args: [{
                selector: 'mat-faq',
                template: `
    <mat-toolbar color="primary">{{title}}</mat-toolbar>
    <mat-accordion [displayMode]="displayMode" [multi]="multi"
                   class="faq-container">
      <mat-expansion-panel *ngFor="let faqItem of faqList">
        <mat-expansion-panel-header>{{faqItem.question}}</mat-expansion-panel-header>
        <p>{{faqItem.answer}}</p>
      </mat-expansion-panel>
    </mat-accordion>
  `,
                styles: [`

  `]
            },] },
];
MatFaqComponent.propDecorators = {
    title: [{ type: Input }],
    multi: [{ type: Input }],
    displayMode: [{ type: Input }],
    faqList: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MatFaqModule {
    /**
     * @return {?}
     */
    static forRoot() {
        return {
            ngModule: MatFaqModule,
            providers: []
        };
    }
}
MatFaqModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    FormsModule,
                    ReactiveFormsModule,
                    FlexLayoutModule,
                    MatCardModule,
                    MatToolbarModule,
                    MatButtonModule,
                    MatInputModule,
                    MatIconModule,
                    MatExpansionModule
                ],
                exports: [
                    MatFaqAdminComponent,
                    MatFaqComponent
                ],
                declarations: [MatFaqAdminComponent, MatFaqComponent]
            },] },
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { MatFaqAdminComponent, MatFaqComponent, MatFaqModule };
//# sourceMappingURL=faq.js.map
