import { Component, OnInit } from '@angular/core';
import {ConsumptionCoordinationService} from '../consumption-coordination.service';

@Component({
  selector: 'app-hybrid',
  templateUrl: './hybrid.component.html',
  styleUrls: ['./hybrid.component.css']
})
export class HybridComponent implements OnInit {
  averageConsumption: any;
  consumption: any;
  fullChargePrice: any;
  countOfFullCharges = 1;
  countOfKmRemaining = 0;
  savingCountOfKmRemainingInMoney: any;
  distance: any;
  maxDistanceEOnly: any;
  hybridFullPrice: any;
  hybridNormalizedPrice: any;
  fuelPrice: any;
  style: number;
  outputColor: any;

  constructor(public OnlyCombutionVsOnlyEServiceRef: ConsumptionCoordinationService) {
    this.OnlyCombutionVsOnlyEServiceRef.registerHybridConsumptionCallBack( () => {
      this.recoverVariablesFromLocalStorage();
      this.calculate();
      this.outputColor = OnlyCombutionVsOnlyEServiceRef.getColor('hybridConsumption');
    });
  }

  ngOnInit(): void {
    this.recoverVariablesFromLocalStorage();
    this.inputChanged();
  }

  private recoverVariablesFromLocalStorage(): void {
    if (localStorage.getItem('dataElectric') != null)
    {
      const json = JSON.parse(localStorage.getItem('dataElectric'));

      this.fullChargePrice = json.fullChargePrice;
      this.maxDistanceEOnly = json.maxDistanceEOnly;
    }

    if (localStorage.getItem('dataHybrid') != null)
    {
      const json = JSON.parse(localStorage.getItem('dataHybrid'));

      this.distance = json.distance;
      this.averageConsumption = json.averageConsumption;
      this.countOfFullCharges = json.countOfFullCharges;
      this.countOfKmRemaining = json.countOfKmRemaining;
    }


    if (localStorage.getItem('dataCombustion') != null)
    {
      const json = JSON.parse(localStorage.getItem('dataCombustion'));

      this.fuelPrice = json.fuelPrice;
    }
  }

  private saveVariablesToLocalStorage(data): void {
    localStorage.setItem('dataHybrid', JSON.stringify(data));
  }


  inputChanged(): void {
    this.calculate();

    this.saveVariablesToLocalStorage({
      distance: this.distance,
      averageConsumption: this.averageConsumption,
      countOfFullCharges: this.countOfFullCharges,
      countOfKmRemaining: this.countOfKmRemaining,
    });

    // consumption is NaN if one of two values is not yet a number
    if (!isNaN(this.hybridNormalizedPrice))
    {
      this.OnlyCombutionVsOnlyEServiceRef.hybridConsumption = this.hybridNormalizedPrice;
      this.OnlyCombutionVsOnlyEServiceRef.updateUi();
    }

    this.recoverVariablesFromLocalStorage();
  }

  private calculate(): void{

    this.consumption = this.fuelPrice * this.averageConsumption;
    this.savingCountOfKmRemainingInMoney = this.countOfKmRemaining / this.maxDistanceEOnly * this.fullChargePrice;
    this.hybridFullPrice = this.consumption / 100 * this.distance + (this.fullChargePrice * this.countOfFullCharges) - this.savingCountOfKmRemainingInMoney;
    this.hybridNormalizedPrice = this.hybridFullPrice / (this.distance / 100);

    if (isNaN(this.hybridFullPrice))
    {
      this.hybridFullPrice = 'Please fill other flields to get a correct calculation.';
      this.hybridNormalizedPrice = 'Please fill other flields to get a correct calculation.';
    }
  }
}
