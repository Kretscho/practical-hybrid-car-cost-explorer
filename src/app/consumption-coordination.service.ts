import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConsumptionCoordinationService {

  private _eConsumption: any;
  private _combustionConsumption: any;
  private _hybridConsumption: any;
  private eConsumptionCallBack: () => void;
  private combustionConsumptionCallBack: () => void;
  private hybridConsumptionCallBack: () => void;


  private targetDistanceConsumptionCallBack: () => void;
  private _eConsumptionToTargetDistance: any;
  private _combustionConsumptionToTargetDistance: any;
  private _hybridConsumptionToTargetDistance: any;
  private _targetDistance: number;

  constructor() {
  }

  public registerEConsumptionCallBack(callback): void
  {
    this.eConsumptionCallBack = callback;
  }

  public registerCombustionConsumptionCallBack(callback): void
  {
    this.combustionConsumptionCallBack = callback;
  }

  public registerHybridConsumptionCallBack(callback): void
  {
    this.hybridConsumptionCallBack = callback;
  }

  public updateUi(): void
  {
    if (typeof this._eConsumption === 'number' &&
      typeof this._combustionConsumption === 'number')
    {
      this.eConsumptionCallBack();
      this.combustionConsumptionCallBack();
    }

    if (typeof this._hybridConsumption === 'number')
    {
      this.hybridConsumptionCallBack();
    }

    if (typeof this._eConsumption === 'number' &&
      typeof this._combustionConsumption === 'number' &&
      typeof this._hybridConsumption === 'number'
    )
    {
      this.targetDistanceConsumptionCallBack();
    }
  }

  get hybridConsumption(): any {
    return this._hybridConsumption;
  }

  set hybridConsumption(value: any) {
    this._hybridConsumption = value;
  }
  get combustionConsumption(): any {
    return this._combustionConsumption;
  }

  set combustionConsumption(value: any) {
    this._combustionConsumption = value;
  }
  get eConsumption(): any {
    return this._eConsumption;
  }

  set eConsumption(value: any) {
    this._eConsumption = value;
  }

  get hybridConsumptionToTargetDistance(): any {
    return this._hybridConsumptionToTargetDistance;
  }

  set hybridConsumptionToTargetDistance(value: any) {
    this._hybridConsumptionToTargetDistance = value;
  }
  get combustionConsumptionToTargetDistance(): any {
    return this._combustionConsumptionToTargetDistance;
  }

  set combustionConsumptionToTargetDistance(value: any) {
    this._combustionConsumptionToTargetDistance = value;
  }
  get eConsumptionToTargetDistance(): any {
    return this._eConsumptionToTargetDistance;
  }

  set eConsumptionToTargetDistance(value: any) {
    this._eConsumptionToTargetDistance = value;
  }

  get targetDistance(): number {
    return this._targetDistance;
  }

  set targetDistance(value: number) {
    this._targetDistance = value;
  }
  public registerTargetDistanceConsumptionCallBack(callback): void{
    this.targetDistanceConsumptionCallBack = callback;
  }

  public getColor(searchTerm: string): string {

    this.calculateConsumptionPriceOnTargetDistance(200);

    let tupleList: [number, string] [];
    tupleList = [
      [this.eConsumption, 'eConsumption'],
      [this.combustionConsumption, 'combustionConsumption'],
      [this.hybridConsumption, 'hybridConsumption']
    ];

    const sorted = tupleList.sort((a, b) => a[0] < b[0] ? 1 : -1);

    if (searchTerm === sorted[0][1])
    {
      return '#dc0c0c'; // green
    }
    else if (searchTerm === sorted[1][1] )
    {
      return '#ffa800'; // orange
    }
    return '#14b914'; // red
  }

  public calculateConsumptionPriceOnTargetDistance(distance: number): void{
    this._eConsumptionToTargetDistance = this.eConsumption / 100 * distance;
    this._combustionConsumptionToTargetDistance = this.combustionConsumption / 100 * distance;
    this._hybridConsumptionToTargetDistance = this.hybridConsumption / 100 * distance;
    this._targetDistance = distance;
  }

}
