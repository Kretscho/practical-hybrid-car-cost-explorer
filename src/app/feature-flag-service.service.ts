import {Injectable} from '@angular/core';
import {
  IMutableContext,
  LocalStorageProvider,
  UnleashClient,
  EVENTS
} from 'unleash-proxy-client';

@Injectable({
  providedIn: 'root'
})

export class FeatureFlagService {

  unleash = new UnleashClient({
    url: `http://localhost:3000/proxy`, // url to Unleash Proxy
    clientKey: `my_secret_for_client`, // secret used in docker env UNLEASH_PROXY_SECRETS
    appName: `development`, // the appName used in docker env UNLEASH_APP_NAME
    environment: `development`, // you can configure it easily in environment.ts and environment.prod.ts
    refreshInterval: 10,
    storageProvider: new LocalStorageProvider(),
  });


  constructor() {
  }

  start(): void {
    this.unleash.start();
    this.unleash.on(EVENTS.ERROR, () => {this.unleash.stop();});

    // 1. this is important to load pre cached data from in memory storage!
    // 2. afterward we emit the update event since other program parts explicitly listen to that.
    this.unleash.on('initialized', () => {
      // console.log("initialized!!"); //console.log(this.unleash.getAllToggles()); // For debugging the in mem
      this.unleash.emit('update');
    });
  }

  stop(): void {
    this.unleash.stop();
  }

  updateContext(contextObj: IMutableContext): void {
    this.unleash.updateContext(contextObj);
  }

  isEnabled(feature: string): boolean {
    return this.unleash.isEnabled(feature);
  }

  subscribe(event: string, callback: Function): void {
    this.unleash.on(event, callback);
  }


}
