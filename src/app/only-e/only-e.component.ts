import { Component, OnInit } from '@angular/core';
import {OnlyCombustionComponent} from '../only-combustion/only-combustion.component';
import {ConsumptionCoordinationService} from '../consumption-coordination.service';

@Component({
  selector: 'app-only-e',
  templateUrl: './only-e.component.html',
  styleUrls: ['./only-e.component.css'],
  providers: []
})
export class OnlyEComponent implements OnInit {
  maxDistanceEOnly: any;
  fullChargePrice: any;
  consumption = NaN;
  outputColor: any;

  constructor(public OnlyCombutionVsOnlyEServiceRef: ConsumptionCoordinationService) {

    // recover cached values
    this.recoverVariablesFromLocalStorage();

    // recover result
    this.consumption = this.calculateConsumption(this.fullChargePrice, this.maxDistanceEOnly);

    // register callback
    this.OnlyCombutionVsOnlyEServiceRef.registerEConsumptionCallBack( () => {
      this.outputColor = this.OnlyCombutionVsOnlyEServiceRef.getColor('eConsumption');
    });

    // apply color if values were cached
    this.inputChanged();
  }

  ngOnInit(): void {
  }

  inputChanged(): void {
    this.consumption = this.calculateConsumption(this.fullChargePrice, this.maxDistanceEOnly);

    this.saveVariablesToLocalStorage({
      fullChargePrice: this.fullChargePrice,
      maxDistanceEOnly: this.maxDistanceEOnly
    });

    // consumption is NaN if one of two values is not yet a number
    if (!isNaN(this.consumption))
    {
      this.OnlyCombutionVsOnlyEServiceRef.eConsumption = this.consumption;
      this.OnlyCombutionVsOnlyEServiceRef.updateUi();
    }

  }

  private saveVariablesToLocalStorage(data): void {
    localStorage.setItem('dataElectric', JSON.stringify(data));
  }

  private recoverVariablesFromLocalStorage(): void {
    if (localStorage.getItem('dataElectric') != null)
    {
      const json = JSON.parse(localStorage.getItem('dataElectric'));

      this.fullChargePrice = json.fullChargePrice;
      this.maxDistanceEOnly = json.maxDistanceEOnly;
    }
  }

  private calculateConsumption(fullChargePrice, maxDistanceEOnly): number {
    return (fullChargePrice / maxDistanceEOnly) * 100;
  }
}
