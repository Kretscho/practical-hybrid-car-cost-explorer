import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlyEComponent } from './only-e.component';

describe('OnlyEComponent', () => {
  let component: OnlyEComponent;
  let fixture: ComponentFixture<OnlyEComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnlyEComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlyEComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
