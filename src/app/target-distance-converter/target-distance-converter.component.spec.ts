import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TargetDistanceConverterComponent } from './target-distance-converter.component';

describe('TargetDistanceConverterComponent', () => {
  let component: TargetDistanceConverterComponent;
  let fixture: ComponentFixture<TargetDistanceConverterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TargetDistanceConverterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TargetDistanceConverterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
