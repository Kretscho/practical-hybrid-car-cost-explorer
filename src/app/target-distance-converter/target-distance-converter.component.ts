import { Component, OnInit } from '@angular/core';
import {ConsumptionCoordinationService} from "../consumption-coordination.service";

export interface PeriodicElement {
  name: string;
  distance: number;
  result: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {name: 'Only Combustion Motor', distance: 0, result: 0},
  {name: 'Only Electric Motor ', distance: 0, result: 0},
  {name: 'Hybrid mode ', distance: 0, result: 0},
];

@Component({
  selector: 'app-target-distance-converter',
  templateUrl: './target-distance-converter.component.html',
  styleUrls: ['./target-distance-converter.component.css']
})


export class TargetDistanceConverterComponent implements OnInit {
  targetDistance: any;

  dataSource = ELEMENT_DATA;
  displayedColumns: string[] = ['name', 'distance', 'result'];

  constructor(public OnlyCombutionVsOnlyEServiceRef: ConsumptionCoordinationService ) {

    OnlyCombutionVsOnlyEServiceRef.registerTargetDistanceConsumptionCallBack( () => {
      this.inputChanged();
    });
  }

  ngOnInit(): void {
  }

  inputChanged() {
    this.OnlyCombutionVsOnlyEServiceRef.calculateConsumptionPriceOnTargetDistance(this.targetDistance);

    ELEMENT_DATA[0].distance = this.targetDistance;
    ELEMENT_DATA[1].distance = this.targetDistance;
    ELEMENT_DATA[2].distance = this.targetDistance;

    ELEMENT_DATA[0].result = this.OnlyCombutionVsOnlyEServiceRef.combustionConsumptionToTargetDistance;
    ELEMENT_DATA[1].result = this.OnlyCombutionVsOnlyEServiceRef.eConsumptionToTargetDistance;
    ELEMENT_DATA[2].result = this.OnlyCombutionVsOnlyEServiceRef.hybridConsumptionToTargetDistance;
  }
}
