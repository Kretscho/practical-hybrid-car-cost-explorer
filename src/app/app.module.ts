import {NgModule} from '@angular/core';
import {BrowserModule } from '@angular/platform-browser';

import {AppComponent } from './app.component';
import {BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {FormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {TwoDigitDecimaNumberDirective } from './only-combustion/two-digit-decima-number.directive';
import {MatCardModule} from '@angular/material/card';
import {OnlyEComponent } from './only-e/only-e.component';
import {MatDividerModule} from '@angular/material/divider';
import {OnlyCombustionComponent } from './only-combustion/only-combustion.component';
import {ServiceWorkerModule } from '@angular/service-worker';
import {environment } from '../environments/environment';
import { WelcomeScreenComponent } from './welcome-screen/welcome-screen.component';
import { FaqComponent } from './faq/faq.component';
import {MatIconModule} from '@angular/material/icon';
import { FooterComponent } from './footer/footer.component';
import {MatButtonModule} from '@angular/material/button';
import { HybridComponent } from './hybrid/hybrid.component';
import {MatExpansionModule} from "@angular/material/expansion";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import { TargetDistanceConverterComponent } from './target-distance-converter/target-distance-converter.component';
import {MatTableModule} from "@angular/material/table";
import {NgcCookieConsentModule, NgcCookieConsentConfig, NgcCookieConsentService} from 'ngx-cookieconsent';


const cookieConfig: NgcCookieConsentConfig = {
  cookie: {
    domain: 'localhost'
  },
  position: 'bottom-left',
  theme: 'classic',
  palette: {
    popup: {
      background: '#000000',
      text: '#ffffff'
    },
    button: {
      background: '#f4d504',
      text: '#000000'
    }
  },
  type: 'info',
  content: {
    message: 'This website uses cookies to ensure you get the best experience on our website.',
    dismiss: 'Got it!',
    deny: 'Refuse cookies',
    link: 'Learn more',
    href: 'https://cookiesandyou.com',
    policy: 'Cookie Policy'
  }
};

@NgModule({
  declarations: [
    AppComponent,
    TwoDigitDecimaNumberDirective,
    OnlyEComponent,
    OnlyCombustionComponent,
    WelcomeScreenComponent,
    FaqComponent,
    FooterComponent,
    HybridComponent,
    TargetDistanceConverterComponent,
  ],
  imports: [
    NgcCookieConsentModule.forRoot(cookieConfig),
    BrowserModule,
    MatTableModule,
    MatButtonModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDividerModule,
    FormsModule,
    FlexLayoutModule,
    MatCardModule,
    MatIconModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor(private ccService: NgcCookieConsentService) {

  }

}
