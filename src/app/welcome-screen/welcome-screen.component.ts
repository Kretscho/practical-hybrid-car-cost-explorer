import {Component, OnInit} from '@angular/core';
import {FeatureFlagService} from '../feature-flag-service.service';

@Component({
  selector: 'app-welcome-screen',
  templateUrl: './welcome-screen.component.html',
  styleUrls: ['./welcome-screen.component.css']
})
export class WelcomeScreenComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {}
}
