import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlyCombustionComponent } from './only-combustion.component';

describe('OnlyCombustionComponent', () => {
  let component: OnlyCombustionComponent;
  let fixture: ComponentFixture<OnlyCombustionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnlyCombustionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlyCombustionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
