import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {OnlyEComponent} from '../only-e/only-e.component';
import {ConsumptionCoordinationService} from '../consumption-coordination.service';

@Component({
  selector: 'app-only-combustion',
  templateUrl: './only-combustion.component.html',
  styleUrls: ['./only-combustion.component.css'],
  providers:  [ ]

})
export class OnlyCombustionComponent implements OnInit {
  //@ViewChild('outputConsumption') input: ElementRef;
  //this.input.nativeElement.style.color = "#14b914";
  averageConsumption: any;
  fuelPrice: any;
  consumption = NaN;
  outputColor: any;

  constructor(public OnlyCombutionVsOnlyEServiceRef : ConsumptionCoordinationService) {

    // recover cached values
    this.recoverVariablesFromLocalStorage();

    // recover result
    this.consumption = this.calculateConsumption(this.fuelPrice, this.averageConsumption);

    this.OnlyCombutionVsOnlyEServiceRef.registerCombustionConsumptionCallBack( () => {
      this.outputColor = this.OnlyCombutionVsOnlyEServiceRef.getColor('combustionConsumption');
    });

    // apply color if values were cached
    this.inputChanged();
  }

  ngOnInit(): void {
  }

  inputChanged(): void {
    this.consumption = this.calculateConsumption(this.fuelPrice, this.averageConsumption);

    this.saveVariablesToLocalStorage({
      fuelPrice: this.fuelPrice,
      averageConsumption: this.averageConsumption
    });

    if(!isNaN(this.consumption)) {
      this.OnlyCombutionVsOnlyEServiceRef.combustionConsumption = this.consumption;
      this.OnlyCombutionVsOnlyEServiceRef.updateUi();
    }

  }

  private saveVariablesToLocalStorage(data): void {
    localStorage.setItem('dataCombustion', JSON.stringify(data));
  }

  private recoverVariablesFromLocalStorage(): void {
    if (localStorage.getItem('dataCombustion') != null)
    {
      const json = JSON.parse(localStorage.getItem('dataCombustion'));

      this.fuelPrice = json.fuelPrice;
      this.averageConsumption = json.averageConsumption;
    }
  }

  private calculateConsumption(fuelPrice, averageConsumption): number {
    return fuelPrice * averageConsumption;
  }



}
