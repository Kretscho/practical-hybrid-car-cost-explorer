import {Component, OnInit} from '@angular/core';
import {FeatureFlagService} from './feature-flag-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  constructor(private readonly featureFlagService: FeatureFlagService) {}
  title = 'hybrid';
  isFaqHidden = 'block'; // new default enabled

  ngOnInit(): void {
    // this.featureFlagService.subscribe('update', () => {
    //   if (this.featureFlagService.isEnabled('faq')) {
    //     this.isFaqHidden = 'block';
    //   } else {
    //     this.isFaqHidden = 'none';
    //   }
    // });

    // Support for translated cookies messages
    // this.translateService.addLangs(['en', 'fr']);
    // this.translateService.setDefaultLang('en');
  }



}
