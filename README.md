# Practical Hybrid Car Cost Explorer

> This app helps to quickly estimate and compare the economic cost-effectiveness of your hybrid vehicle's combustion and electric engine.

Check out the running app on [practical-hybrid-car-cost-explorer.com](https://practical-hybrid-car-cost-explorer.com/).

If you have any issues feel free to open a _[service request](mailto:contact-project+kretscho-practical-hybrid-car-cost-explorer-37818885-issue-@incoming.gitlab.com)_ or create an[ issue](https://gitlab.com/Kretscho/practical-hybrid-car-cost-explorer/-/issues).

If you like the app, feel free to [buy me a coffee](https://www.buymeacoffee.com/Kretsch0) and boost creativeness ;) 
